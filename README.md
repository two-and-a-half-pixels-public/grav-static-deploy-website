# Grav Static Deploy Website

Base for a [Grav](https://getgrav.org) CMS based website that will be automatically be deployed to giltab page as static html pages.

## Website Authoring

The website is being authored on a local machine running a local webserver. All Changes that are pushed to gitlab will be automatically be deployed straight away to gitlab pages.

1. Clone repository
   ```
   $ git clone git@gitlab.com:two-and-a-half-pixels-public/grav-static-deploy-website.git **website-name**
   ```
2. Run grav installation
   ```
   $ cd **website-name**
   $ bin/grav install
   ```
3. Run local webserver with docker
   ```
   $ docker run -v **website-full-path**:/var/www/html:cached -p 80:80 benjick/grav
   ```
4. Open http://localhost to verify your installation is working

## Website Live Deployment

1. Push your changes to the gitlab origin
2. Done

## License

See [LICENSE](LICENSE.txt)
